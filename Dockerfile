FROM nginx:1.19-alpine

LABEL maintainer="Dušan Simić <dusan.simic1810@gmail.com>"

WORKDIR /usr/share/nginx/html

RUN echo -e "http://dl-cdn.alpinelinux.org/alpine/edge/main\n\
http://dl-cdn.alpinelinux.org/alpine/edge/community\n\
http://dl-cdn.alpinelinux.org/alpine/edge/testing" > /etc/apk/repositories

RUN apk add --no-cache createrepo_c

COPY ./entry.sh /docker-entrypoint.d/30-createrepo-entry.sh

RUN chmod +x /docker-entrypoint.d/30-createrepo-entry.sh
