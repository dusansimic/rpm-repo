# RPM Repo Server

Run your own RPM repository using Docker. This is meant to be used as a small repo open to the internet, not as a local repo. For making local repos, look [here](https://wiki.centos.org/HowTos/CreateLocalRepos).

## Usage

To run the server, cd into the path contaning rpm that you want to host and run the container by adding the volume to the nginx html dir. Here is an example.

```sh
docker run -p 8080:80 -v $(pwd):/usr/share/nginx/html dusansimic/rpm-repo
```

If you're running the container with podman, add `:Z` option ad the end of volume parameter like this.

```sh
podman run -p 8080:80 -v $(pwd):/usr/share/nginx/html:Z dusansimic/rpm-repo
```
